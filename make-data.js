const casual = require('casual');
const fs = require('fs');

const categories = [
  {
    id: casual.uuid,
    title: 'Truyện Tranh',
    slug: 'truyen-tranh',
    description:
      'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Totam, nemo sequi praesentium provident illo incidunt enim a facere laudantium ratione.',
    titleSeo: 'Đọc Truyện Tranh Online',
    descSeo:
      'Đọc Truyện Tranh Online Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae, numquam?',
    image: `https://picsum.photos/id/${casual.integer(1, 1000)}/1000/800`,
    visible: true,
    createdAt: Date.now(),
    updatedAt: Date.now(),
  },
  {
    id: casual.uuid,
    title: 'Truyện Kiếm Hiệp',
    slug: 'truyen-kiem-hiep',
    description:
      'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Totam, nemo sequi praesentium provident illo incidunt enim a facere laudantium ratione.',
    titleSeo: 'Đọc Kiếm Hiệp Online',
    descSeo:
      'Đọc Kiếm Hiệp Online Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae, numquam?',
    image: `https://picsum.photos/id/${casual.integer(1, 1000)}/1000/800`,
    visible: true,
    createdAt: Date.now(),
    updatedAt: Date.now(),
  },
  {
    id: casual.uuid,
    title: 'Truyện Tiên Hiệp',
    slug: 'truyen-tien-hiep',
    description:
      'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Totam, nemo sequi praesentium provident illo incidunt enim a facere laudantium ratione.',
    titleSeo: 'Đọc Tiên Hiệp Online',
    descSeo:
      'Đọc Tiên Hiệp Online Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae, numquam?',
    image: `https://picsum.photos/id/${casual.integer(1, 1000)}/1000/800`,
    visible: true,
    createdAt: Date.now(),
    updatedAt: Date.now(),
  },
  {
    id: casual.uuid,
    title: 'Truyện Ngôn Tình',
    slug: 'truyen-ngon-tinh',
    description:
      'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Totam, nemo sequi praesentium provident illo incidunt enim a facere laudantium ratione.',
    titleSeo: 'Đọc Ngôn Tình Online',
    descSeo:
      'Đọc Ngôn Tình Online Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae, numquam?',
    image: `https://picsum.photos/id/${casual.integer(1, 1000)}/1000/800`,
    visible: true,
    createdAt: Date.now(),
    updatedAt: Date.now(),
  },
  {
    id: casual.uuid,
    title: 'Truyện Đam Mỹ',
    slug: 'truyen-dam-my',
    description:
      'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Totam, nemo sequi praesentium provident illo incidunt enim a facere laudantium ratione.',
    titleSeo: 'Đọc Đam Mỹ Online',
    descSeo:
      'Đọc Đam Mỹ Online Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae, numquam?',
    image: `https://picsum.photos/id/${casual.integer(1, 1000)}/1000/800`,
    visible: true,
    createdAt: Date.now(),
    updatedAt: Date.now(),
  },
];

// Random 50 stories data
const stories = [];
Array.from(new Array(20).keys()).map(() => {
  const story = {
    id: casual.uuid,
    title: casual.title,
    slug: '',
    description: casual.words(200),
    titleSeo: casual.title,
    descSeo: casual.words(20),
    image: `https://picsum.photos/id/${casual.integer(1, 1000)}/1000/800`,
    author: casual.full_name,
    category: '',
    genres: [],
    visible: true,
    popular: false,
    completed: false,
    createdAt: Date.now(),
    updatedAt: Date.now(),
  };

  stories.push(story);
});

// Random 50 chaps data
const chaps = [];
Array.from(new Array(20).keys()).map(() => {
  const chap = {
    id: casual.uuid,
    title: casual.title,
    slug: '',
    description: casual.words(500),
    images: [],
    story: '',
    visible: true,
    popular: false,
    createdAt: Date.now(),
    updatedAt: Date.now(),
  };
  chaps.push(chap);
});
// --------------------
const main = async () => {
  // Setup db object
  const db = {
    categories,
    stories,
    chaps,
  };

  // Save posts array to db.json file
  fs.writeFile('db.json', JSON.stringify(db), () => {
    console.log(`Generate saved in db.json!!! =))`);
  });
};
main();
